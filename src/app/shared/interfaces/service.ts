export interface Service {
  id: string;
  title: string;
  slug: string;
  catID: string;
  img?: string;
  description?: string;
}
