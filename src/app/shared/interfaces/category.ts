export interface Category {
  id: string;
  title: string;
  slug: string;
  description?: string;
}
