import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { Category } from '../interfaces/category';
import { Service } from '../interfaces/service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})

export class DataStorageService {
  private categoriesCollection: AngularFirestoreCollection<Category>;
  private servicesCollection: AngularFirestoreCollection<Service>;

  private categoriesBasePath = '/categories';
  private servicesBasePath = '/services';

  idChanged = new Subject<string>();


  constructor(
    private readonly afs: AngularFirestore,
    private router: Router,
    private mateTitle: Title) {
    this.categoriesCollection = this.afs.collection<Category>(this.categoriesBasePath);
    this.servicesCollection = this.afs.collection<Service>(this.servicesBasePath);
  }

  setId(id: string) {
    this.idChanged.next(id);
  }

  getCategory(slug: string): Observable<Category> {
    return this.getCategories().pipe(
      // check if slug exists
      map(categories => {
        const categoryExists = categories.find(category => category.slug === slug);

        if (categoryExists) {
          // set ID to check exist services in the category
          this.setId(categoryExists.id);
          this.mateTitle.setTitle(categoryExists.title);
          return categoryExists;
        } else {
          this.router.navigate(['/404']);
        }
      })
    );
  }

  // check the parent slug of the service
  getCategorySlug(id: string) {
    if (id) {
      return this.afs.doc<Category>(`${this.categoriesBasePath}/${id}`).valueChanges();
    }
  }

  getService(slug: string): Observable<Service> {
    return this.servicesCollection.snapshotChanges().pipe(
      map(services => {
        const serviceData = services.find(service => service.payload.doc.data().slug === slug);

        if (serviceData && serviceData.payload) {
          return serviceData.payload.doc.data();
        } else {
          this.router.navigate(['/404']);
        }
      })
    );
  }

  getCategories(): Observable<Category[]> {
    return this.categoriesCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Category;
          const id = a.payload.doc.id;

          return { id, ...data };
        });
      })
    );
  }

  getServices(catID: string): Observable<Service[]> {
    return this.servicesCollection.snapshotChanges().pipe(
      map(services => {
        const serviceInCategory = [];

        // check exist services in category
        services.forEach(service => {
          if (service.payload.doc.data().catID === catID) {
            serviceInCategory.push(service.payload.doc.data());
          }
        });
        return serviceInCategory;
      })
    );
  }

}
