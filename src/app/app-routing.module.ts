import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './static-pages/home/home.component';
import { CategoriesComponent } from './categories/categories.component';
import { ServiceComponent } from './categories/service/service.component';
import { OnasComponent } from './static-pages/onas/onas.component';
import { FaqComponent } from './static-pages/faq/faq.component';
import { TarifComponent } from './static-pages/tarif/tarif.component';
import { BezopasnostComponent } from './static-pages/bezopasnost/bezopasnost.component';
import { LegalComponent } from './static-pages/legal/legal.component';
import { PageNotFoundComponent } from './error-pages/page-not-found/page-not-found.component';
import { InternalServerComponent } from './error-pages/internal-server/internal-server.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { CategoryComponent } from './categories/category/category.component';


const routes: Routes = [
  { path: '',  children: [
    {path: '', component: HomeComponent},
    { path: 'onas', component: OnasComponent },
    { path: 'faq', component: FaqComponent },
    { path: 'tarif', component: TarifComponent },
    { path: 'bezopasnost', component: BezopasnostComponent },
    { path: 'legal', component: LegalComponent },
    { path: '500', component: InternalServerComponent },
    { path: '404', component: PageNotFoundComponent },
    { path: ':slug', component: CategoriesComponent, children: [
      {path: '', component: CategoryComponent},
      {path: ':slug', component: ServiceComponent }
    ]},
  ]},
  { path: 'signin', component: SigninComponent, outlet: 'modal' },
  { path: 'signup', component: SignupComponent, outlet: 'modal' },
  { path: '**', redirectTo: '/404', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
