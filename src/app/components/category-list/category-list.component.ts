import { Component, OnInit } from '@angular/core';
import { DataStorageService } from 'src/app/shared/services/data-storage.service';
import { Observable } from 'rxjs';
import { Category } from 'src/app/shared/interfaces/category';
import { ErrorHandlerService } from 'src/app/shared/services/error-handler.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {
  $categoryList: Observable<Category[]>;
  spinner = true;
  errorMessage = '';

  constructor(
    private dataStorageService: DataStorageService,
    private errorHandlerService: ErrorHandlerService) { }

  ngOnInit() {
    this.$categoryList = this.dataStorageService.getCategories();
    this.$categoryList.subscribe(
      () => this.spinner = false,
      error => {
        this.errorHandlerService.handleError(error);
        this.errorMessage = this.errorHandlerService.errorMessage;
      }
    );
  }
}
