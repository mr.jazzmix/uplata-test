import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss']
})
export class LegalComponent implements OnInit {
  title = 'Правовая информация';

  constructor(private mateTitle: Title) { }

  ngOnInit() {
    this.mateTitle.setTitle(this.title);
  }

}
