import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-onas',
  templateUrl: './onas.component.html',
  styleUrls: ['./onas.component.scss']
})
export class OnasComponent implements OnInit {
  title = 'О Нас';

  constructor(private mateTitle: Title) { }

  ngOnInit() {
    this.mateTitle.setTitle(this.title);
  }

}
