import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-bezopasnost',
  templateUrl: './bezopasnost.component.html',
  styleUrls: ['./bezopasnost.component.scss']
})
export class BezopasnostComponent implements OnInit {
  title = 'Безопасность';

  constructor(private mateTitle: Title) { }

  ngOnInit() {
    this.mateTitle.setTitle(this.title);
  }

}
