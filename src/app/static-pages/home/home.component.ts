import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title = 'ТЗ для Front End';

  constructor(private mateTitle: Title) { }

  ngOnInit() {
    this.mateTitle.setTitle(this.title);
  }

}
