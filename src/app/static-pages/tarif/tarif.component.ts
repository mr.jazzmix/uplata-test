import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-tarif',
  templateUrl: './tarif.component.html',
  styleUrls: ['./tarif.component.scss']
})
export class TarifComponent implements OnInit {
  title = 'Тарифы';

  constructor(private mateTitle: Title) { }

  ngOnInit() {
    this.mateTitle.setTitle(this.title);
  }

}
