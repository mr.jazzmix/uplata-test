import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoriesComponent } from './categories/categories.component';
import { OnasComponent } from './static-pages/onas/onas.component';
import { FaqComponent } from './static-pages/faq/faq.component';
import { TarifComponent } from './static-pages/tarif/tarif.component';
import { BezopasnostComponent } from './static-pages/bezopasnost/bezopasnost.component';
import { LegalComponent } from './static-pages/legal/legal.component';
import { PageNotFoundComponent } from './error-pages/page-not-found/page-not-found.component';
import { InternalServerComponent } from './error-pages/internal-server/internal-server.component';
import { ServiceComponent } from './categories/service/service.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { HomeComponent } from './static-pages/home/home.component';
import { CategoryComponent } from './categories/category/category.component';


@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    OnasComponent,
    FaqComponent,
    TarifComponent,
    BezopasnostComponent,
    LegalComponent,
    PageNotFoundComponent,
    InternalServerComponent,
    ServiceComponent,
    NavbarComponent,
    SigninComponent,
    SignupComponent,
    CategoryListComponent,
    HomeComponent,
    CategoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }
