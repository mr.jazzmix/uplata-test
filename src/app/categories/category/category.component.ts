import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { DataStorageService } from 'src/app/shared/services/data-storage.service';
import { Category } from 'src/app/shared/interfaces/category';
import { ErrorHandlerService } from 'src/app/shared/services/error-handler.service';
import { Service } from 'src/app/shared/interfaces/service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy {
  $category: Observable<Category>;
  $servicesInCategory: Observable<Service[]>;

  routeSubscription: Subscription;
  categorySubscription: Subscription;
  idChangedSubscription: Subscription;

  errorMessage = '';
  spinner = true;

  constructor(
    private dataStorageService: DataStorageService,
    private route: ActivatedRoute,
    private errorHandlerService: ErrorHandlerService) { }

  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe(
      (params: Params) => {
        this.$category = this.dataStorageService.getCategory(params.slug);
      }
    );

    this.categorySubscription = this.$category.subscribe(
      () => this.spinner = false,
      error => {
        this.errorHandlerService.handleError(error);
        this.errorMessage = this.errorHandlerService.errorMessage;
      }
    );

    this.idChangedSubscription = this.dataStorageService.idChanged.subscribe(catID => {
      this.$servicesInCategory = this.dataStorageService.getServices(catID);
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
    this.categorySubscription.unsubscribe();
    this.idChangedSubscription.unsubscribe();
  }

}
