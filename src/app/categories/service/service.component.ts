import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

import { DataStorageService } from 'src/app/shared/services/data-storage.service';
import { Service } from 'src/app/shared/interfaces/service';
import { Category } from 'src/app/shared/interfaces/category';
import { ErrorHandlerService } from 'src/app/shared/services/error-handler.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent implements OnInit, OnDestroy {
  $service: Observable<Service>;
  $categorySlug: Observable<Category>;

  categorySlugSubscription: Subscription;
  serviceSubscription: Subscription;
  routeSubscription: Subscription;
  idChangedSubscription: Subscription;

  errorMessage = '';
  spinner = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dataStorageService: DataStorageService,
    private errorHandlerService: ErrorHandlerService,
    private mateTitle: Title) { }

  ngOnInit() {

    this.routeSubscription = this.route.params.subscribe((params: Params) => {
      this.$service = this.dataStorageService.getService(params.slug);
    });

    this.serviceSubscription = this.$service.subscribe(
      props => {
        if (props && props.catID) {
          this.dataStorageService.setId(props.catID);
          this.mateTitle.setTitle(props.title);
          this.spinner = false;
        }
      },
      error => {
        this.errorHandlerService.handleError(error);
        this.errorMessage = this.errorHandlerService.errorMessage;
      }
    );

    this.idChangedSubscription = this.dataStorageService.idChanged.subscribe(id => {
      setTimeout(() => {
        this.$categorySlug = this.dataStorageService.getCategorySlug(id);
        if (this.$categorySlug) {
          this.categorySlugSubscription = this.$categorySlug.subscribe(category => {
            const parentSlug = this.router.url.split('/', 2).join('');

            if (category.slug !== parentSlug) {
              this.router.navigate(['/404']);
            }
          });
        }
      });
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
    this.serviceSubscription.unsubscribe();
    this.idChangedSubscription.unsubscribe();
    if (this.categorySlugSubscription) {
      this.categorySlugSubscription.unsubscribe();
    }
  }
}
