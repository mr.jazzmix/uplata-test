# посмотреть тут: [DEMO](https://uplata-test.now.sh/)

Данные приходят из firebase в 2-х коллекциях:

categories
![categories](https://i.ibb.co/3pVWTTk/categories.png)

services
![services](https://i.ibb.co/D9dQLPw/services.png)


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
